# Firefox Stable on Debian

No one likes ESR

## Install
* Run as `./install-firefox.sh`

## Reference
* https://wiki.debian.org/Firefox

## Caution
There are no checks in place. There is nothing to prevent writting over existing files or directories. This script assumes a default Debian/Kali environment.
